package com.loveholidays.interview.services;

import com.loveholidays.interview.models.Address;

public interface AddressMapper<T> {

    Address map(final T t);
}
