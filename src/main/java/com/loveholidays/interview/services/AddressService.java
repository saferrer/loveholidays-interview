package com.loveholidays.interview.services;

import com.loveholidays.interview.models.Address;

public interface AddressService {

    Address getBy(String postcode) throws AddressServiceException;
}
