package com.loveholidays.interview.services.openaddressesuk;

import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressMapper;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkAddress;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkResponse;

class OpenAddressesUkAddressMapper implements AddressMapper<OpenAddressesUkResponse> {

    public Address map(final OpenAddressesUkResponse openAddressesUkResponse) {

        OpenAddressesUkAddress address = openAddressesUkResponse.getAddresses().stream().findFirst().get();

        return Address.builder().street(address.getStreet().getName().get("en").get(0)).number(address.getPao()).build();
    }
}