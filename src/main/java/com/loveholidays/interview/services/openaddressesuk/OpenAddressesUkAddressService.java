package com.loveholidays.interview.services.openaddressesuk;

import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressService;
import com.loveholidays.interview.services.AddressServiceException;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

@Service
public class OpenAddressesUkAddressService implements AddressService {

    private static final String BASE_URL = "https://alpha.openaddressesuk.org/addresses.json";

    private final RestTemplate client;
    private final OpenAddressesUkAddressMapper mapper;

    OpenAddressesUkAddressService(RestTemplate client, OpenAddressesUkAddressMapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    @Autowired
    public OpenAddressesUkAddressService(RestTemplate client) {
        this(client, new OpenAddressesUkAddressMapper());
    }

    @Override
    public Address getBy(String postcode) throws AddressServiceException {
        UriComponentsBuilder builder = fromUriString(BASE_URL)
                .queryParam("postcode", postcode);

        try {
            OpenAddressesUkResponse craftyClicksAddress = client.getForEntity(builder.build().toUri(), OpenAddressesUkResponse.class).getBody();
            if (craftyClicksAddress.getAddresses().isEmpty()) {
                return null;
            }

            return mapper.map(craftyClicksAddress);
        } catch (RestClientException e) {
            throw new AddressServiceException(e);
        }
    }
}
