package com.loveholidays.interview.services.openaddressesuk.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenAddressesUkResponse {

    @Getter
    @Setter
    @JsonProperty
    private List<OpenAddressesUkAddress> addresses;

}
