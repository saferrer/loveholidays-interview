package com.loveholidays.interview.services.openaddressesuk.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenAddressesUkStreet {

    @JsonProperty
    private Map<String, List<String>> name;

}