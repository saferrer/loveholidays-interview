package com.loveholidays.interview.services.openaddressesuk.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenAddressesUkAddress {

    @JsonProperty
    private String pao;

    @JsonProperty
    private OpenAddressesUkStreet street;

}