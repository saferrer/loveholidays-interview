package com.loveholidays.interview.services;

public class AddressServiceException extends Exception {

    public AddressServiceException() {
        super("Error getting address");
    }

    public AddressServiceException(Throwable cause) {
        super("Error getting address", cause);
    }
}
