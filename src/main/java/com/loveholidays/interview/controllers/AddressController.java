package com.loveholidays.interview.controllers;

import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressService;
import com.loveholidays.interview.services.AddressServiceException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

;

@RestController
@AllArgsConstructor
public class AddressController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressController.class);

    @Autowired
    private final AddressService addressService;

    @RequestMapping(value = "/addresses", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity find(@RequestParam("postcode") String postcode) {
        try {
            Address address = addressService.getBy(postcode);

            return address == null ? new ResponseEntity(NOT_FOUND) : ok(address);
        } catch (AddressServiceException e) {
            LOGGER.error("Error", e);
            return new ResponseEntity(INTERNAL_SERVER_ERROR);
        }
    }

}
