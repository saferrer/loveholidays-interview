package com.loveholidays.interview.services.openaddressesuk;

import com.google.common.collect.ImmutableMap;
import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressServiceException;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkAddress;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkResponse;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkStreet;
import org.junit.Test;

import java.net.URISyntaxException;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class OpenAddressesUkAddressMapperTest {

    private OpenAddressesUkAddressMapper testInstance = new OpenAddressesUkAddressMapper();

    @Test
    public void returnAddressWhenValidOpenAddressesUkResponse() throws AddressServiceException, URISyntaxException {
        Address expectedAddress = Address.builder().number("10").street("DOWNING STREET").build();

        OpenAddressesUkResponse openAddressesUkResponse = validOpenAddressesUkResponse("10", "DOWNING STREET");

        assertThat(testInstance.map(openAddressesUkResponse)).isEqualTo(expectedAddress);
    }

    @Test
    public void ThrowsExceptionWhenInvalidOpenAddressesUkResponse() throws AddressServiceException, URISyntaxException {
        OpenAddressesUkResponse invalidOpenAddressesUkResponse = new OpenAddressesUkResponse();

        Throwable thrown = catchThrowable(() -> testInstance.map(invalidOpenAddressesUkResponse));

        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void ThrowsExceptionWhenNullOpenAddressesUkResponse() throws AddressServiceException, URISyntaxException {
        Throwable thrown = catchThrowable(() -> testInstance.map(null));

        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

    private OpenAddressesUkResponse validOpenAddressesUkResponse(String number, String street) {
        OpenAddressesUkResponse openAddressesUkResponse = new OpenAddressesUkResponse();
        OpenAddressesUkAddress openAddressesUkAddress = new OpenAddressesUkAddress();
        openAddressesUkAddress.setPao(number);
        OpenAddressesUkStreet openAddressesUkStreet = new OpenAddressesUkStreet();
        openAddressesUkStreet.setName(ImmutableMap.of("en", singletonList(street)));
        openAddressesUkAddress.setStreet(openAddressesUkStreet);
        openAddressesUkResponse.setAddresses(singletonList(openAddressesUkAddress));

        return openAddressesUkResponse;
    }
}
