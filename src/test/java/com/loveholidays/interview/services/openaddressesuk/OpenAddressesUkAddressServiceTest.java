package com.loveholidays.interview.services.openaddressesuk;

import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressServiceException;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkAddress;
import com.loveholidays.interview.services.openaddressesuk.dtos.OpenAddressesUkResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.ResponseEntity.ok;

@RunWith(MockitoJUnitRunner.class)
public class OpenAddressesUkAddressServiceTest {

    @Mock
    RestTemplate restTemplate;

    @Mock
    OpenAddressesUkAddressMapper mapper;

    @InjectMocks
    OpenAddressesUkAddressService testInstance;

    @Test
    public void returnAddressWhenRestClientAndMapperWorksAsExpected() throws AddressServiceException, URISyntaxException {
        String postcode = "111";
        Address expectedAddress = Address.builder().build();
        OpenAddressesUkResponse openAddressesUkResponse = new OpenAddressesUkResponse();
        openAddressesUkResponse.setAddresses(singletonList(new OpenAddressesUkAddress()));

        given(restTemplate.getForEntity(new URI("https://alpha.openaddressesuk.org/addresses.json?postcode=" + postcode), OpenAddressesUkResponse.class))
                .willReturn(ok(openAddressesUkResponse));
        given(mapper.map(openAddressesUkResponse)).willReturn(expectedAddress);

        assertThat(testInstance.getBy(postcode)).isEqualTo(expectedAddress);
    }

    @Test
    public void returnNullAddressWhenNoAddressFound() throws AddressServiceException, URISyntaxException {
        String postcode = "111";
        OpenAddressesUkResponse openAddressesUkResponse = new OpenAddressesUkResponse();
        openAddressesUkResponse.setAddresses(emptyList());

        given(restTemplate.getForEntity(new URI("https://alpha.openaddressesuk.org/addresses.json?postcode=" + postcode), OpenAddressesUkResponse.class))
                .willReturn(ok(openAddressesUkResponse));

        assertThat(testInstance.getBy(postcode)).isNull();
    }

    @Test
    public void throwAddressServiceExceptionWhenRestClientException() throws AddressServiceException, URISyntaxException {
        String postcode = "111";

        given(restTemplate.getForEntity(new URI("https://alpha.openaddressesuk.org/addresses.json?postcode=" + postcode), OpenAddressesUkResponse.class))
                .willThrow(new RestClientException("Error"));

        Throwable thrown = catchThrowable(() -> testInstance.getBy(postcode));
        assertThat(thrown).isInstanceOf(AddressServiceException.class);
    }
}
