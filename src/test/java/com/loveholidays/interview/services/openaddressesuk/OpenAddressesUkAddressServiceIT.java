package com.loveholidays.interview.services.openaddressesuk;

import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressServiceException;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import static org.assertj.core.api.Assertions.assertThat;

public class OpenAddressesUkAddressServiceIT {

    private OpenAddressesUkAddressService testInstance;

    @Before
    public void setUp() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        testInstance = new OpenAddressesUkAddressService(restTemplate(), new OpenAddressesUkAddressMapper());
    }

    @Test
    public void returnAddressGivenAValidPostCode() throws AddressServiceException, URISyntaxException {
        String postcode = "SW1A2AA";

        Address expectedAddress = Address.builder().number("10").street("DOWNING STREET").build();

        assertThat(testInstance.getBy(postcode)).isEqualTo(expectedAddress);
    }

    @Test
    public void returnsNullGivenAnInValidPostCode() throws AddressServiceException, URISyntaxException {
        String postcode = "N42GP";

        assertThat(testInstance.getBy(postcode)).isNull();
    }

    private RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        return new RestTemplate(requestFactory);
    }
}
