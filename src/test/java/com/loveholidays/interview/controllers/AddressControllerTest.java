package com.loveholidays.interview.controllers;

import com.loveholidays.interview.models.Address;
import com.loveholidays.interview.services.AddressService;
import com.loveholidays.interview.services.AddressServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.springframework.http.HttpStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class AddressControllerTest {

    @Mock
    AddressService addressService;

    @InjectMocks
    AddressController testInstance;

    @Test
    public void returnOkWhenAddressServiceComesWithAnAddress() throws AddressServiceException {
        String postcode = "1111";
        Address expectedAddress = Address.builder().build();
        given(addressService.getBy(eq(postcode))).willReturn(expectedAddress);

        ResponseEntity responseEntity = testInstance.find(postcode);

        assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
        assertThat(responseEntity.getBody()).isEqualTo(expectedAddress);
    }

    @Test
    public void returnNotFoundWhenAddressServiceComesWithNoAddress() throws AddressServiceException {
        String postcode = "1111";
        given(addressService.getBy(eq(postcode))).willReturn(null);

        ResponseEntity responseEntity = testInstance.find(postcode);

        assertThat(responseEntity.getStatusCode()).isEqualTo(NOT_FOUND);
        assertThat(responseEntity.getBody()).isNull();
    }

    @Test
    public void returnInternalServerErrorWhenAddressServiceThrowsAddressServiceException() throws AddressServiceException {
        String postcode = "1111";
        given(addressService.getBy(eq(postcode))).willThrow(new AddressServiceException());

        ResponseEntity responseEntity = testInstance.find(postcode);

        assertThat(responseEntity.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
        assertThat(responseEntity.getBody()).isNull();
    }
}
